package com.example.bombrunner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Results extends AppCompatActivity {

    ListView simpleList;
    String[] nameArray = {"2020-03-03: Success","2020-03-15: Success","2020-04-02: Success","2020-04-20: Success","2020-04-29: Failure","2020-05-05: Success" };

    String[] infoArray = {
            "Distance: 2.2 km. Time: 12 min",
            "Distance: 5 km. Time: 32 min",
            "Distance: 1 km. Time: 7 min",
            "Distance: 4 km. Time: 22 min",
            "Distance: 4 km. Time: 30 min",
            "Distance: 1.5 km. Time: 15 min"
    };
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        CustomListAdapter whatever = new CustomListAdapter(this, nameArray, infoArray);
        listView = (ListView) findViewById(R.id.simpleListView);
        listView.setAdapter(whatever);

    }
}
