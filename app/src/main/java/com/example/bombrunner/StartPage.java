package com.example.bombrunner;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class StartPage extends AppCompatActivity {

    private Button StartBtn;
    private Button ResultBtn;
  //  private Button explodeBtn;
    private Button TutorialBtn;
    private MediaPlayer click;
    private Vibrator v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_page);

        StartBtn = findViewById(R.id.startRun);
        ResultBtn = findViewById(R.id.results);
        TutorialBtn = findViewById(R.id.howtoplay);
    //    explodeBtn = findViewById(R.id.btn_explode);

        StartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v){
                startRun();
            }
        });

        ResultBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v){
                showResultPage();
            }
        });
        TutorialBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v){
                tutorialPage();         }
        });
       /* explodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                explode();
            }
        }); */

        click = MediaPlayer.create(this, R.raw.click);
        v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
    }

    public void startRun() {
        click.start();
        v.vibrate(50);
        Intent intent = new Intent(this, SetupActivity.class);
        startActivity(intent);
    }

    public void showResultPage() {
        click.start();
        v.vibrate(50);
        Intent intent = new Intent(this, Results.class);
        startActivity(intent);
    }

    public void tutorialPage() {
        click.start();
        v.vibrate(50);
        Intent intent = new Intent(this, TutorialActivity.class);
        startActivity(intent);
    }

/*    public void explode () {
        Intent intent = new Intent (this, ExplodeActivity.class);
        intent.putExtra("source", "start");
        startActivity(intent);
    } */
}
