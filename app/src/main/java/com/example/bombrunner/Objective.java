package com.example.bombrunner;

public abstract class Objective {

    protected double x, y;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
