package com.example.bombrunner;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.sql.Time;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class SensorService extends Service implements SensorEventListener {
    // These are time between peaks (millis)
    private static final long shake = 10;
    private static final long run = 450;
    private static final long jump = 1000;

    private long lastTime;

    private SensorManager sensorManager;
    private Sensor accelerometer;
    private boolean foundAccelerometer;

    private String exercise;
    private int jumps;
    private float[] vAccelerometer;

    private static final float SHAKE_THRESHOLD_GRAVITY = 2.7F;
    private static final int SHAKE_SLOP_TIME_MS = 1000;
    private long mShakeTimestamp;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        exercise = "Finding exercise!";
        jumps = 0;
        foundAccelerometer = false;
        vAccelerometer = new float[3];
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        lastTime = System.currentTimeMillis();

        if (sensorManager != null) {
            System.out.println("Sensor manager found!");
            if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
                System.out.println("Accelerometer found!");
                accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
                foundAccelerometer = sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
            }
        } else {
            System.out.println("No accelerometer");
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            System.arraycopy(event.values, 0, vAccelerometer, 0, event.values.length);
            // System.out.println("Accelorometer triggered! ____-----_____-----_____------");
            determineMovement(vAccelerometer);
        }

    }

    // "Rules":
        // Shake:
            // y ~= 0.8z
            // 10 < z < 20
            // Fast interval between peaks
        // Jump:
            // z ~= 0.5y
            // 15 < y <= 30
            // Long interval between peaks
        // Run:
            // y and z < 20
            // Medium interval between peaks (hard to program?)

    private void determineMovement (float[] values) {
        exercise = "";

        if(shake(values)){
            exercise = "Shake";
            sendMovement(exercise);
            //System.out.println("*******Shake*****");
            //Log.d("sensor", "shake detected w/ speed: ");
        }

        if(values[1] > 17) { // && values[1] <= 30
            long time = System.currentTimeMillis();

            if ((time - lastTime) >= jump) {
                exercise = "Jump";
                jumps++;
                lastTime = time;
                sendMovement(exercise);
            }
        }

        if (exercise.equals("")) {
            exercise = "None";
            sendMovement(exercise);
        }
    }

    private void sendMovement (String exercise) {
        Intent intent = new Intent ();
        intent.setAction("movement");
        intent.putExtra("exercise", exercise);
        intent.putExtra("jumps", jumps);
        sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sensorManager.unregisterListener(this, accelerometer);
    }

    private boolean shake(float[] values){


            float x = values[0];
            float y = values[1];
            float z = values[2];

            float gX = x / SensorManager.GRAVITY_EARTH;
            float gY = y / SensorManager.GRAVITY_EARTH;
            float gZ = z / SensorManager.GRAVITY_EARTH;

            // gForce will be close to 1 when there is no movement.
            float gForce = (float) Math.sqrt(gX * gX + gY * gY + gZ * gZ);

            if (gForce > SHAKE_THRESHOLD_GRAVITY) {
                final long now = System.currentTimeMillis();
                // ignore shake events too close to each other (500ms)
                if (mShakeTimestamp + SHAKE_SLOP_TIME_MS > now) {
                    return false;
                }

                mShakeTimestamp = now;

                return true;
            }

            return false;

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}
}
