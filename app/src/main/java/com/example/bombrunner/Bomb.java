package com.example.bombrunner;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class Bomb extends Objective implements Serializable {


    private ArrayList <Hint> hints; //Varför public? Borde väl räcka med att det finns en
    private boolean disarmed = false;

    public Bomb(double x, double y, ArrayList<Hint> hints) { //antal ledtrådar i bomben, x och y-koordinater för bomben (där rutan startar och slutar).
        this.hints = hints; //arraylist för ledtrådarna?
        this.x = x;
        this.y = y;
    }

    public ArrayList<Hint> getHints(){
        return hints;
    }

    public boolean bombRedy(){
        for(Hint hint : hints){
            if(!hint.isFound()){
                return false;
            }
        }
        return true;
    }

    public boolean isDisarmed(){
        return disarmed;
    }

    public void disarm(){

        disarmed = true;
    }
}
