package com.example.bombrunner;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;

public class ExplodeActivity extends AppCompatActivity {

    private Button continueBtn;
    private Button quitBtn;

    // These values originate from the map
    GameController gc;
    long timePassed;
    int steps;
    int generatedDistance;

    private MediaPlayer mySound;
    private Vibrator v;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explode);
        v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        mySound = MediaPlayer.create(this, R.raw.explosion); //kan byta till ett bätte ljud.
        Intent intent = getIntent();
        // The if-statement is there so the test works!
        if (intent.getStringExtra("source").equals("map")) {
            gc = (GameController) intent.getExtras().getSerializable("gc");
            timePassed = intent.getLongExtra("time", 0); // milliseconds
            steps = intent.getIntExtra("steps", 0);
            generatedDistance = intent.getIntExtra("distance", 0);
        }

        setModalSize(.8f, .6f);
        setUpButtons ();
      //  mySound.start();
      //  v.vibrate(500);
    }

    private void setModalSize (float percentX, float percentY) {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width * percentX), (int)(height * percentY));
    }

    private void setUpButtons () {
        continueBtn = findViewById(R.id.btn_continue);
        quitBtn = findViewById(R.id.btn_quit);

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                continueRun();
            }
        });

        quitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quitRun();
            }
        });
    }

    public void continueRun () {
        // Go back to map
        // Start counting time plus the time passed
        // Count steps plus steps already taken
        // Count distance plus distance already moved.

        finish ();
    }

    public void quitRun () {
        Intent intent = new Intent(this, TimeOutActivity.class);
        intent.putExtra("gc", gc);
        intent.putExtra("steps", steps);
        intent.putExtra("time", timePassed);
        intent.putExtra("distance", generatedDistance);

        startActivity(intent);
    }
}
