package com.example.bombrunner;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.Timer;
import java.util.TimerTask;

public class TimerService extends Service {

    private long millis;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                millis += 1000;
                sendPassedTime();
            }
        }, 1000, 1000);

        return super.onStartCommand(intent, flags, startId);
    }

    private void sendPassedTime () {
        Intent intent = new Intent ();
        intent.setAction("timePassed");
        intent.putExtra("timePassed", millis);
        sendBroadcast(intent);
    }
}
