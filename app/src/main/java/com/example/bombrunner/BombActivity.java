package com.example.bombrunner;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class BombActivity extends AppCompatActivity {


    private GameController gameController; //Ska inte skapas här men gör det nu för att kunna testa
    private Bomb bomb;
    private Vibrator v;
    private MediaPlayer mySound;
    private int steps;
    private Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bomb);
        intent = getIntent();
        intent.clone();

        v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        mySound = MediaPlayer.create(this, R.raw.defused); //kan byta till ett bätte ljud.
        gameController = (GameController) intent.getExtras().getSerializable("gc");

        bomb = gameController.getBomb();


    }



    public void onDisarm(View view){
        v.vibrate(200);
        bomb.disarm();
        mySound.start();
        intent.setClass(this, TimeOutActivity.class);
        intent.putExtra("gc", gameController);
        startActivity(intent);
    }


}
