package com.example.bombrunner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MapsActivity extends AppCompatActivity
        implements
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        GoogleMap.OnMarkerClickListener,
        OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback,
        SensorEventListener {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    private boolean mPermissionDenied = false;
    private CountDownTimer timer;

    private GameController gc;
    private TextView timerText;
    private GoogleMap mMap;
    private ArrayList<Marker> markers;
    private FusedLocationProviderClient fusedLocationClient;
    private Location startLocation;
    private Location[] hintLocations;
    private Vibrator v;
    private MediaPlayer mySound;
    private MediaPlayer fivemin;
    private MediaPlayer onemin;
    private MediaPlayer thirtysec;
    private MediaPlayer explosion;
    private MediaPlayer ticking;
    private MediaPlayer planted;
    private Marker bombMarker;
    private MediaPlayer click;
    private MediaPlayer pause;
    private MediaPlayer resume;
    private MediaPlayer jumping;
    private MediaPlayer mario;


    private long timePassed;
    private long timeLeft;  // Time left in milliseconds
    private int distance;  // Total distance in meters
    private int hints;  // Number of hints
    private BroadcastReceiver timeReceiver;
    private IntentFilter timeIntentFilter;
    private Intent timeServiceIntent;

    private BroadcastReceiver sensorReceiver;
    private IntentFilter sensorIntentFilter;
    private Intent sensorServiceIntent;
    private TextView exerciseTest;
    private int jumps;  // Number of consecutive jumps
    private boolean hintInRange;
    private boolean bombInŔange;

    private StepDetector simpleStepDetector;
    private SensorManager sensorManager;
    private Sensor accel;
    private static final String TEXT_NUM_STEPS = "Number of Steps: ";

    private CountDownTimer cdt;
    private CountDownTimer cdtStart;

    private int numSteps = 0;
    private Polyline mPolyline;
    private int generatedDistance;
    private TextView startingText;
    private long counter;
    private ImageButton imageButton;
    private Switch aSwitch;
    private Button quitBtn;



    //shake
    private int mShakeCount;
    private static final float SHAKE_THRESHOLD_GRAVITY = 2.7F;
    private static final int SHAKE_SLOP_TIME_MS = 1000;
    private static final int SHAKE_COUNT_RESET_TIME_MS = 3000;
    private long mShakeTimestamp;


    private boolean paused = false;
    private boolean cancel =false;

    private static final long shake = 10;
    private static final long run = 450;
    private static final long jump = 1500;

    private long lastTime;

    private Sensor accelerometer;
    private boolean foundAccelerometer;

    private String exercise;
    //private int jumps;
    private float[] vAccelerometer;

    private boolean inside = false;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        Intent intent = getIntent();
        v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

        mySound = MediaPlayer.create(this, R.raw.when); //kan byta till ett bätte ljud.
        fivemin = MediaPlayer.create(this, R.raw.fivemin);
        onemin = MediaPlayer.create(this, R.raw.onemin);
        thirtysec = MediaPlayer.create(this, R.raw.thirtysec);
    /*    tensec = MediaPlayer.create(this, R.raw.tensec);
        nine = MediaPlayer.create(this, R.raw.nine);
        eight = MediaPlayer.create(this, R.raw.eight);
        seven = MediaPlayer.create(this, R.raw.seven);
        six = MediaPlayer.create(this, R.raw.six);
        five = MediaPlayer.create(this, R.raw.five);
        four = MediaPlayer.create(this, R.raw.four);
        three = MediaPlayer.create(this, R.raw.three);
        two = MediaPlayer.create(this, R.raw.two);
        one = MediaPlayer.create(this, R.raw.one); */
        explosion = MediaPlayer.create(this, R.raw.explosion);
        ticking = MediaPlayer.create(this, R.raw.ticking);
        planted = MediaPlayer.create(this, R.raw.planted);
        click = MediaPlayer.create(this, R.raw.click);
        pause = MediaPlayer.create(this, R.raw.pause);
        resume = MediaPlayer.create(this, R.raw.resume);
        jumping = MediaPlayer.create(this, R.raw.jumping);
        mario = MediaPlayer.create(this, R.raw.mario);
        imageButton = findViewById(R.id.imageButton);
        quitBtn = findViewById(R.id.quitButton);
        aSwitch = findViewById(R.id.switch1);
        timeLeft = Long.parseLong(intent.getStringExtra("Duration")) * 60000; // Time left in milliseconds
        distance = Integer.parseInt(intent.getStringExtra("Distance"));
        hints = Integer.parseInt(intent.getStringExtra("Hints"));

        timerText = findViewById(R.id.timer); // Gets the timer text from layout
        gc = (GameController) intent.getExtras().getSerializable("GameController");

        //Stepcounter:  Get an instance of the SensorManager
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        simpleStepDetector = new StepDetector();
        simpleStepDetector.registerListener(this);




        quitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quit();
            }
        });



        //  startingText = (TextView) findViewById(R.id.startText);

        sensorManager.registerListener(MapsActivity.this, accel, SensorManager.SENSOR_DELAY_FASTEST);

        // Total time counter:
        ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.FOREGROUND_SERVICE}, PackageManager.PERMISSION_GRANTED);
        timeServiceIntent = new Intent (this, TimerService.class);
        startService(timeServiceIntent);
        setupBroadcastReceivers();
      //  exerciseTest = findViewById(R.id.txt_test_exercise);

        jumps = 0;
        sensorServiceIntent = new Intent (this, SensorService.class);
        startService(sensorServiceIntent);

        // Sensor service, permission already granted
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        enableMyLocation();
        markers = new ArrayList<>();
     //   timerText.setText((int)timeLeft);
        setupTargets(gc.getNbrOfHints(), gc.getDistance());
        mMap.setOnMarkerClickListener(this);
        //  drawRoute();
//        moveCamera();
    }

    /**
     * Animates the camera to current location
     */
    private void moveCamera() {
        fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    LatLng coordinates = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 15));
                }
            }
        });
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
            }
        } else {
            // Permission to access the location is missing. Show rationale and request permission
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    private void setupBroadcastReceivers () {
        timeIntentFilter = new IntentFilter();
        timeIntentFilter.addAction("timePassed");
        timeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                timePassed = intent.getLongExtra("timePassed", 0);
                // System.out.println("Time passed: " + timePassed + "____________-------------__________--------");
            }
        };

       /* sensorIntentFilter = new IntentFilter();
        sensorIntentFilter.addAction("movement");
        sensorReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                checkForHints();    // Call this to get accurate distance measurement

                if (!(hintInRange || bombInŔange)) {   // Jumps reset if moving away from objectives
                    jumps = 0;
                }

                if (!intent.getStringExtra("exercise").equals("None") && (hintInRange || bombInŔange)) {
                    if (!bombInŔange && hintInRange) {
                        jumps++;
                    }

                    if (bombInŔange) { // 10 jumps to disarm
                        jumps++;
                        if (!gc.getBomb().isDisarmed() && jumps > 9) {
                            disarmBomb(gc.getBomb());
                            jumps = 0;
                        }
                    }
                }

                if(intent.getStringExtra("exercise").equals("Shake") && !hintInRange){
                    if(aSwitch.isChecked()) {
                        pausePlay();
                    }
                }

           //     exerciseTest.setText(Integer.toString(jumps));
            }
        };*/

        registerReceiver(timeReceiver, timeIntentFilter); // don't forget to unregister these!
        //registerReceiver(sensorReceiver, sensorIntentFilter);
    }





    private void determineMovement (float[] values) {
        exercise = "";
        long time = System.currentTimeMillis();

        if((time - lastTime) >= jump){
        if(shake(values)){
            exercise = "Shake";
            sendMovement(exercise);
            lastTime = time;
            //System.out.println("*******Shake*****");
            //Log.d("sensor", "shake detected w/ speed: ");
        } else if(values[1] > 17) { // && values[1] <= 30
            exercise = "Jump";
            sendMovement(exercise);
            lastTime = time;


            /*if ((time - lastTime) >= jump) {

                lastTime = time;

            }*/
        }
        }

        /*if (exercise.equals("")) {
            exercise = "None";
            sendMovement(exercise);
        }*/
    }

    private void sendMovement (String exercise) {

        //checkForHints();    // Call this to get accurate distance measurement

        if (!(hintInRange || bombInŔange)) {   // Jumps reset if moving away from objectives
            jumps = 0;
            inside =false;
        }

        else if (!exercise.equals("None") && (hintInRange || bombInŔange)) {
            if (!bombInŔange && hintInRange) {
                mario.start();
                jumps++;

            }
            if (bombInŔange) { // 10 jumps to disarm
                mario.start();
                jumps++;

                if (!gc.getBomb().isDisarmed() && jumps > 9) {


                    disarmBomb(gc.getBomb());
                    jumps = 0;
                }
            }
        }

        if(exercise.equals("Shake") && !hintInRange){
            if(aSwitch.isChecked()) {
                pausePlay();
            }
        }


        //Toast.makeText(this, Integer.toString(jumps), Toast.LENGTH_SHORT).show();


    }

    private boolean shake(float[] values){


        float x = values[0];
        float y = values[1];
        float z = values[2];

        float gX = x / SensorManager.GRAVITY_EARTH;
        float gY = y / SensorManager.GRAVITY_EARTH;
        float gZ = z / SensorManager.GRAVITY_EARTH;

        // gForce will be close to 1 when there is no movement.
        float gForce = (float) Math.sqrt(gX * gX + gY * gY + gZ * gZ);

        if (gForce > SHAKE_THRESHOLD_GRAVITY) {
            final long now = System.currentTimeMillis();
            // ignore shake events too close to each other (500ms)
            if (mShakeTimestamp + SHAKE_SLOP_TIME_MS > now) {
                return false;
            }

            mShakeTimestamp = now;

            return true;
        }

        return false;

    }

    private void disarmBomb (Bomb bomb) {
        //Toast.makeText(this, "Bomb disarmed", Toast.LENGTH_SHORT).show();
        bomb.disarm();

        Intent intent = new Intent (this, TimeOutActivity.class);
        intent.putExtra("steps", numSteps);
        intent.putExtra("time", timePassed);
        intent.putExtra("gc", gc);

        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(timeReceiver);
        //unregisterReceiver(sensorReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(timeReceiver, timeIntentFilter);
        //registerReceiver(sensorReceiver, sensorIntentFilter);
    }

    public void startTimer() {
        cdt = new CountDownTimer(timeLeft, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (paused || cancel) {
                    cancel();
                } else {

                    timeLeft = millisUntilFinished;
                    updateTimer();
                    checkForHints(); //Anropar metoden här för att den ska köras varje sekund, kanske ska lägga den någon annanstans sen.
                }
            }

            @Override
            public void onFinish() {
                //    bomb.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.bomb_explode));
                finishTimer();
            }
        }.start();
    }

    private void finishTimer() {
        Intent timeIntent = new Intent(this, ExplodeActivity.class);
        timeIntent.putExtra("steps", numSteps);
        timeIntent.putExtra("time", timePassed);
        timeIntent.putExtra("gc", gc);
        timeIntent.putExtra("distance", generatedDistance); // Change this to moved distance when implemented!
        timeIntent.putExtra("source", "map");
        // Put extra intent data
        bombMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.bomb_explode_mini));
        startActivity(timeIntent);


    }

    private void updateTimer() {
        int minutes = (int) timeLeft / 60000;
        int seconds = (int) (timeLeft % 60000) / 1000;

        String timeLeftText = minutes + ":";
        if (seconds < 10) timeLeftText += "0";  // We always want 2 digits to display seconds
        timeLeftText += seconds;
        timerText.setText(timeLeftText);
     /*   if(minutes==5 && seconds==0) { //5 min kvar
            fivemin.start();
            v.vibrate(50);
        }else if(minutes==1 && seconds==0) {//1 min kvar
            onemin.start();
            v.vibrate(50);
        } else if(minutes==0 && seconds==30) {//30 sek kvar
            thirtysec.start();
            v.vibrate(50);
        } else */
            if(minutes==0 && seconds==10){ //10 sek kvar
            ticking.start();
            v.vibrate(50);
            //starta vibration
        } else if(minutes==0 && seconds==9){
      //      nine.start(); //byt ljud
            v.vibrate(100);
         } else if(minutes==0 && seconds==8) {
        //    eight.start(); //byt ljud
            v.vibrate(100);
    } else if(minutes==0 && seconds==7) {
         //   seven.start(); //byt ljud
            v.vibrate(100);
    } else if(minutes==0 && seconds==6) {
         //   six.start(); //byt ljud
            v.vibrate(100);
    } else if(minutes==0 && seconds==5) {
            ticking.start();
         //   five.start(); //byt ljud
            v.vibrate(100);
        } else if(minutes==0 && seconds==4) {
         //   four.start(); //byt ljud
            v.vibrate(100);
    } else if(minutes==0 && seconds==3) {
          //  three.start(); //byt ljud
            v.vibrate(100);
        } else if(minutes==0 && seconds==2) {
          //  two.start(); //byt ljud
            v.vibrate(100);
        } else if(minutes==0 && seconds==1) {
          //  one.start(); //byt ljud
            v.vibrate(100);
        } else if (minutes==0 && seconds==0){
            explosion.start();
            v.vibrate(100);
        //    bomb.setVisible(false);

        }


    }

    private void setInRange (boolean bool) {
        hintInRange = bool;
        System.out.println("isIngRange: " + bool);
    }

    public void checkForHints() {
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        int hintsInRange = 0;
                        int hintsFound = 0;

                        if (gc.getBomb().getHints().size() > 0) {
                            for (Hint h : gc.getBomb().getHints()) {
                                double dist = checkDistanceToObjective(location, h);
                                System.out.println(dist + " TO " + h.getid());
                                if (checkDistanceToObjective(location, h) < 20 && !h.isFound()) {// Kan ändra avståndet här. 20 från början, 100 för testning
                                    if(!inside){
                                        jumping.start();
                                        inside =true;
                                    }
                                    hintsInRange++;
                                    if (jumps > 4) {    // Ändra siffran för fler eller färre repetitioner
                                        inside =false;
                                        jumps = 0;
                                        h.setFound();
                                        v.vibrate(100);
                                        mySound.start();
                                        for (Marker m : markers) {
                                            if (m.getTitle().equals(h.getid())) {
                                                m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.hintfound));
                                            }
                                        }
                                    }
                                }
                                if (h.isFound()) {
                                    hintsFound++;
                                }
                            }

                            if (hintsInRange >= 1) setInRange(true);
                            else setInRange(false);

                            bombInŔange = (hintsFound >= gc.getBomb().getHints().size() && checkDistanceToObjective(location, gc.getBomb()) < 20);
                        }
                    }
                });
    }

    public double checkDistanceToObjective(Location location, Objective o) { //kollar avståndet till en hint, x1,y1 är min plats, x2,y2 är hintens plats.
        double x1 = location.getLatitude();
        double y1 = location.getLongitude();
        double x2 = o.getX();
        double y2 = o.getY();
        double dist = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
        double meters = dist * 100000; //gör om till meter.
        return meters;
    }

    public void showBomb() { //Använder endast tillfälligt för att kunna se bombsidan, när det går att nå bomben från kartan tar jag bort
        cancel = true;

        Intent intent = new Intent(this, BombActivity.class);
        intent.putExtra("steps", numSteps);
        intent.putExtra("time", timePassed);
        intent.putExtra("gc", gc);
        intent.putExtra("distance", generatedDistance); // Change this to moved distance when implemented!
        intent.putExtra("source", "map");

        startActivity(intent);

    }

    // https://stackoverflow.com/questions/43469290/generating-random-position-near-me-with-markers
    // https://developers.google.com/maps/documentation/android-sdk/marker
    public void setupTargets(final int amount, final int distance) {
        System.out.println("Setting up targets");
        hintLocations = new Location[amount*2];
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        startLocation = location;
                        int radius = (int) Math.round(distance/(2*Math.PI));
                        //creates bomb on startlocation
                        gc.createBomb(amount, startLocation.getLatitude(), startLocation.getLongitude()); //bomben skapas.

                        //skapar hintsLocations

                        for (int i = 0; i < amount*2; i++) {
                            /*if (i == 0) {
                                hintLocations[i] = startLocation;   // hint vid start för testning
                            } else {
                                hintLocations[i] = getRandomLocation(radius); //dessa är dem gamla.
                            }*/

                            hintLocations[i] = getRandomLocation(radius);
                        }
//snappedLocations(hintLocations);
                        snapToRoad(hintLocations); //anropar API-et för att hitta närmsta väg för alla punkter

                        if (location != null) {
                            // Logic to handle location object
                        }
                    }

                });
    }

    // https://stackoverflow.com/questions/43469290/generating-random-position-near-me-with-markers
    public Location getRandomLocation(int radius) {
        Location r = new Location("");
        LatLng start = new LatLng(startLocation.getLatitude(), startLocation.getLongitude());
        double radiusInDegrees = radius / 111000f;
        double x0 = start.latitude;
        double y0 = start.longitude;

        Random random = new Random();

        double u = random.nextDouble();
        double v = random.nextDouble();
        double w = radiusInDegrees * Math.sqrt(u);
        double t = 2 * Math.PI * v;
        double x = w * Math.cos(t);
        double y = w * Math.sin(t);

        // Adjust the x-coordinate for the shrinking of the east-west distances
        double new_x = x / Math.cos(y0);

        double foundLatitude = new_x + x0;
        double foundLongitude = y + y0;

        LatLng randomLatLng = new LatLng(foundLatitude, foundLongitude);

        r.setLatitude(randomLatLng.latitude);
        r.setLongitude(randomLatLng.longitude);

        return r;
    }

    public void snappedLocations(Location[] snapped) { //ja bort alla ojämna index
        //skapar hintsLocations
        Location[] filtered = new Location[(snapped.length / 2)];
        int j = 0;
        for (int i = 0; i < snapped.length; i++) {
            if ((i % 2) == 0) {
                filtered[j] = snapped[i];
                //  System.out.println("new snap: index "+j+", "+snapped[i]);
                j++;
            }
        }
        hintLocations = sortMarkers(filtered);
        drawRoute(startLocation, hintLocations);

    }

    public void startGame(){
/*counter = 300;
        cdtStart = new CountDownTimer(counter, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                counter = millisUntilFinished;
                startingText.setText((int)counter); //har lagt till distance för rutten här också bara för att kolla.
            }

            @Override
            public void onFinish() { */
                startTimer();
                moveCamera();
                planted.start();
           // }
       // }.start();




//        räkna ner tre två ett
// startar timern.
    }

    public void pauseGame(){
        //om man klickar på en paus-knapp?
    }


    public void addTargetsToMap() {
        Bomb b = gc.getBomb();
        MarkerOptions bomb = new MarkerOptions().position(new LatLng(b.getX(), b.getY())).title("bomb").icon(BitmapDescriptorFactory.fromResource(R.drawable.nybomb));
        bombMarker = mMap.addMarker(bomb);
        String id = "a";
    //    System.out.println("amount: "+ hintLocations.length);
        for (int i = 0; i < hintLocations.length; i++) {
            if (hintLocations[i] != null) {
                Marker m = mMap.addMarker(new MarkerOptions().position(new LatLng(hintLocations[i].getLatitude(), hintLocations[i].getLongitude())).title(id).icon(BitmapDescriptorFactory.fromResource(R.drawable.hint)));
                markers.add(m);
             //   System.out.println("id: "+id);
                gc.getBomb().getHints().add(gc.createHint(id, hintLocations[i].getLatitude(), hintLocations[i].getLongitude()));
                id = id + "a"; //id är antalet a.
              //  System.out.println(hintLocations[i].getLatitude()+", "+hintLocations[i].getLongitude());
            }
        }

        startGame();
    }

    /**
     * Called when the user clicks a marker.
     */
    @Override
    public boolean onMarkerClick(final Marker marker) {



        return true;
    }


    public Location[] sortMarkers(Location[] hintArray) { //TODO: Implementera detta så det blir kortaste vägen.
        Location[] sortedArray = new Location[hintArray.length]; //båda har samma längd.
     //   System.out.println("sortedarraylength " + sortedArray.length);
        Location origClosest = new Location("");
        origClosest.setLatitude(10000.0);
        origClosest.setLongitude(10000.0);
        double x = (double) gc.getBomb().getX();
        double y = (double) gc.getBomb().getY();
        Location current = new Location("");
        current.setLatitude(x);
        current.setLongitude(y); //current är bomben till att börja med.
        Location bomb = current;
     /*   System.out.println("before sorted: ");
        for (int i = 0; i < hintArray.length; i++) {
            System.out.println(bomb.distanceTo(hintArray[i]));

        } */
        Location[] tempArray = hintArray;
        int index=0;
        Location closest = origClosest; //för att det inte ska bli null.
        for (int j = 0; j < sortedArray.length; j++) {
            for (int i = 0; i < hintArray.length; i++) {
                //   System.out.println("distance to closest: "+current.distanceTo(hintLocations[i]));
                if (tempArray[i] != null) {
                    if (current.distanceTo(hintArray[i]) < current.distanceTo(closest)) {
                        closest = hintArray[i]; //ny closest
                    //    System.out.println("new closest " + closest);
                        index = i;
                    }
                }else {
                        System.out.println("i=" + i + " är redan kollad");
                    }
                }

                sortedArray[j] = closest;
                tempArray[index] = null;
                current = closest;
                closest=origClosest;
           //     System.out.println("closest: " + closest);
            }

          System.out.println("after sorted: ");
        int dist=0;
            for (int i = 0; i < sortedArray.length; i++) {
                System.out.println(sortedArray[i].getLatitude()+", "+sortedArray[i].getLongitude());
                System.out.println("distance from bomb to hint: "+bomb.distanceTo(sortedArray[i]));
            }
            return sortedArray;

    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            //System.arraycopy(sensorEvent.values, 0, vAccelerometer, 0, sensorEvent.values.length);
            simpleStepDetector.updateAccel(
                    sensorEvent.timestamp, sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]);

            // System.out.println("Accelorometer triggered! ____-----_____-----_____------");
            determineMovement(sensorEvent.values);
        }



    }


    public void onSwitch(View view){
        if(aSwitch.isChecked()){
            imageButton.setVisibility(View.GONE);
        }else{
            imageButton.setVisibility(View.VISIBLE);
        }
    }

    public void button_pause (View view) {
        pausePlay();

    }

    public void quit () {
        Intent intent = new Intent(this, TimeOutActivity.class);
        intent.putExtra("steps", numSteps);
        intent.putExtra("time", timePassed);
        intent.putExtra("gc", gc);
        intent.putExtra("distance", generatedDistance); // Change this to moved distance when implemented!
        startActivity(intent);
        cancel = true;

    }

    public void pausePlay(){
        paused = !paused;
        if (!paused) {
            resume.start();
            imageButton.setBackgroundResource(R.drawable.pause2);
            startTimer();
        } else {
            pause.start();
            imageButton.setBackgroundResource(R.drawable.play);
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void step(long timeNs) {
        numSteps++;

    }

    public void drawRoute(Location location, Location[] hintLocations) { //Skicka med Arrayen med hints här.
        // Getting URL to the Google Directions API
        LatLng bomb = new LatLng(location.getLatitude(), location.getLongitude());
    //    System.out.println("drawLocation: " + bomb);
        String url = getDirectionsUrl(bomb, bomb, hintLocations); //ska starta och sluta vid bomben.
        DownloadTask downloadTask = new DownloadTask(this);
       // System.out.println("url: " + url);
        //  Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }

    private void snapToRoad(Location[] loc) { //ändra till en array.
     //   System.out.println("length loc:"+loc.length);

        //tar ut alla hints ut arrayen och lägg dem i url-anropet.
        String points = "&points=";
        for (int i = 0; i < loc.length; i++) {
            System.out.println("in loop");
            if (i == loc.length - 1) {
                points += loc[i].getLatitude() + "," + loc[i].getLongitude();
            } else {
                points += loc[i].getLatitude() + "," + loc[i].getLongitude() + "|";
            }
        }
     //   System.out.println("string points: " + points);
        String url = "https://roads.googleapis.com/v1/nearestRoads?points=" +points+"&key=" + getString(R.string.google_maps_key);
        DownloadRoadTask downloadRoadTask = new DownloadRoadTask(this);
     //   System.out.println("url: " + url);

        //  Start downloading json data from Google Directions API
        downloadRoadTask.execute(url);
    }


   public boolean calcDistance(String result){ //kollar så att rutten som skapas inte avviker för mycket med det som användaren skrev in.
       JSONObject json = null;
       try {
           json = new JSONObject(result);
           JSONArray resultsArray = json.getJSONArray("routes");
           JSONArray legs = resultsArray.getJSONObject(0).getJSONArray("legs");
           int distance = 0;
           for(int i=0; i<legs.length();i++){
               JSONObject leg = legs.getJSONObject(i);
              // System.out.println("stepsObjects: " + leg.toString());
               JSONObject dist = leg.getJSONObject("distance");
           //    System.out.println("Legdist: " + dist.toString());
               int value = (int) dist.get("value");
               distance += value;
           }
           generatedDistance = distance;
           System.out.print("totalt distance of round: "+distance);
           if(Math.abs(distance-gc.getDistance())>(gc.getDistance()*0.2)){
               System.out.println("Skillnaden är för stor, genererar nya platser");
               return false;
           } else {
               System.out.println("Skillnaden är OK");
               return true;
           }
       } catch(Exception e){
           System.out.println(e);
           return false;
       }
   };



    public void clearSetup(){
        for(int i=0; i<markers.size(); i++){
            markers.get(i).remove();
        }
        markers.clear();
        for(int i=0; i<hintLocations.length; i++){
            hintLocations[i] = null;
        }
    }




    /** API-anrop **/

    public String getDirectionsUrl(LatLng origin, LatLng dest, Location[] hints) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "&destination=" + dest.latitude + "," + dest.longitude;
        String str_hints = "&waypoints=";
    //    System.out.println("hinta: "+hints.toString());
        for (int i = 0; i < hints.length; i++) {
            System.out.println("in loop");
            if (i == ((hints.length) - 1)){
                str_hints += hints[i].getLatitude() + "," + hints[i].getLongitude();
            } else {
                str_hints += hints[i].getLatitude() + "," + hints[i].getLongitude() + "|";
            }
        }
      //  System.out.println("string hints: " + str_hints);
        String mode = "&stopover=true&mode=walking";
        String optimise = "&optimizeWaypoints";
        String alternatives = "&alternatives=true";
        String key = "&key=" + getString(R.string.google_maps_key);
        String url = "https://maps.googleapis.com/maps/api/directions/json?" + str_origin + str_dest + str_hints + optimise + mode + alternatives + key;
        return url;

    }




    /**
     * A method to download json data from url
     */
    public String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception on download", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
     //   System.out.println("data in request: " + data);
        return data;
    }





    /**
     * A class to download data from Google Directions URL
     */
    private class DownloadTask extends AsyncTask<String, Void, String> {
        MapsActivity ma;

        public DownloadTask(MapsActivity ma) {
            this.ma = ma;
        }
        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {
            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("DownloadTask", "DownloadTask : " + data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
          System.out.println("running onpostexecute");
            if(!ma.calcDistance(result)) { //om skillnaden är ok, ritas linjerna upp.
                System.out.println("genererar nya");
                clearSetup();
                setupTargets(gc.getNbrOfHints(), gc.getDistance());
            } else {
                //lägga till markers på kartan.
                super.onPostExecute(result);
                ParserTask parserTask = new ParserTask();
                // Invokes the thread for parsing the JSON data
                parserTask.execute(result);
                ma.addTargetsToMap();
             //   ma.startTimer();
            }
        }
    }

    /**
     * A class to parse the Google Directions in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("routes: "+ routes);
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.BLUE);
            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                if (mPolyline != null) {
                    mPolyline.remove();
                }
                mPolyline = mMap.addPolyline(lineOptions);

            } else
                Toast.makeText(getApplicationContext(), "No route is found", Toast.LENGTH_LONG).show();
        }
    }






/** ROADS API REQUEST AND RESPONSE PARSER**/
    /**
     * A method to download json data from url
     */
    private class DownloadRoadTask extends AsyncTask<String, Void, String> {
        protected String data;
        protected LatLng l;
        MapsActivity ma;

        public DownloadRoadTask(MapsActivity ma) {
            this.ma = ma;
        }

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("DownloadTask", "DownloadTask : " + data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
          //  ma.testing(l);
            System.out.println("data url: "+data);
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            System.out.println("result: " + result);
            Location[] myLoc = new Location[gc.getNbrOfHints()*2];
            JSONObject json = null;
            try {
                json = new JSONObject(result);
                JSONArray resultsArray = json.getJSONArray("snappedPoints");
                System.out.println("snappedPoints: " + resultsArray.length());
                for(int i = 0; i<gc.getNbrOfHints()*2; i++) {
                    System.out.println("index: "+i);
                    JSONObject locationsArray = resultsArray.getJSONObject(i).getJSONObject("location");
                    System.out.println("locationArray: " + locationsArray);
                    double lat = (double) locationsArray.get("latitude");
                    double lo = (double) locationsArray.get("longitude");
                    Location location = new Location("");
                    location.setLatitude(lat);
                    location.setLongitude(lo);
                    myLoc[i] = location;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            System.out.println("length myloc: " + myLoc.length);
            ma.snappedLocations(myLoc); //anropar MapsActivitys snappedLocations med nya arrayen.
        }

    }

}








