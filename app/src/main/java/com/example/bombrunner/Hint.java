package com.example.bombrunner;

import java.io.Serializable;

public class Hint extends Objective implements Serializable {

    private Boolean found;
    private String id;

    public Hint(String id, double x, double y) { //ev ha hint id på varje hint?
        this.found = false;  // true för testning
        this.id = id;
        this.x=x;
        this.y=y;
    }

    public Boolean isFound() {
        return found;
    }

    public String getid(){
        return id.toString();
    }

    public void setFound(){
        found = true;
    }

}
