package com.example.bombrunner;

import java.io.Serializable;
import java.util.ArrayList;

//sätter upp funktionaliteten för spelet. antal ledtrådar, distans etc.
public class GameController implements Serializable {
    private int nbrOfHints;
    private int distance; // i km
    private Boolean finishedGame;
    private Bomb bomb;

    public GameController(int nbrOfHints, int distance){ //får in information från StartPage
        this.nbrOfHints = nbrOfHints;
        this.distance = distance*1000;
        this.finishedGame = false;
        //tog bort att en bomb skapades här, men kan egentligen lägga tillbaka det här om createBomb inte gör så mycket mer.
    }

    public void runGame(){
    }

    public Bomb createBomb(int nbr, double x, double y){ //skapar och returnerar en Bomb med ledtrådar och koordinater.
        ArrayList <Hint> list = new ArrayList<Hint>();
  /*      for(int i=0; i<nbr; i++){
            list.add(createHint());
        }*/ // flyttad till MapsActivity för parametrar till createHint skickas med där.
        bomb = new Bomb(x, y, list); //ÄNDRA DETTA: det blev fel när vi bara returnerade new bomb, för det var inte samma som den globala i klassen, blev fel på getBomb() i MapsActivity.
        return bomb; //bomben är har koordinater där rundan börjar
    }

    public Hint createHint(String id, double x, double y){ //skapar och returnerar en ledtråd, med en bokstav och koordinater. Anropas i mapsActivity.
        return new Hint(id, x, y);
    }

    public void checkForBomb(){ //kollar om användaren befinner sig vid bomben och alla ledtrådar är hittade, isåfall visas bomben. kanske ska flyttas till MapsActivity så vi kan kolla koordinaterna?
        double currentX = 0.0; //läs in nuvarande plats
        double currentY = 0.0; //läs in nuvarande plats
        //använd bomb.bombRedy() för att kolla om alla hints är hittade
        if(currentX == bomb.getX() && currentY == bomb.getY() && bomb.bombRedy()){
            showBomb();
            //bomben kommer upp som en popup och användaren kan klicka på disarm
        }
    }

    public void checkForHints(){
        //checkForHints i MapsActivity kanske ska flyttas hit.
    }

    public void showBomb(){
        //öppna BombActivity
        //visar bomben som en popup, användaren kan skriva in bokstäverna.
        //anropar bomb för att kontrollera bokstäverna?
    }

    public void endGame(){
        //ge någon utskrift
        //navigera till resultat-sidan
    }

    public int getNbrOfHints(){
        return nbrOfHints;
    }

    public boolean bombDisarmed(){

        return bomb.isDisarmed();
    }

    public int getDistance(){
        return distance;
    }

    public Bomb getBomb(){
        return bomb;
    }

}
