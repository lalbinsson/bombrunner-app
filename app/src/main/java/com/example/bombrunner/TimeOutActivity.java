package com.example.bombrunner;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TimeOutActivity extends AppCompatActivity {
    private GameController gc;
    private Button homeBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_out);
        homeBtn = findViewById(R.id.homeButton);
        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v){
                showHomePage();
            }
        });

        Intent intent = getIntent();
        int steps = intent.getIntExtra("steps", 0);
        long time = intent.getLongExtra("time", 0); // milliseconds
        System.out.println(steps);

        TextView step = findViewById(R.id.steps);
        step.setText("Number of steps: "+steps);

        TextView timeText = findViewById(R.id.txt_time);
        timeText.setText("Mission duration: " + formatTime(time));

        gc = (GameController) intent.getExtras().getSerializable("gc");
        setSuccess();


    }

    private String formatTime (long time) {
        int hours = (int) time / (60 * 60 * 1000);
        int minutes = (int) (time % (60 * 60 * 1000)) / (60 * 1000);
        int seconds = (int) (time % (60 * 1000)) / 1000;

        String text = "";
        if (hours == 0) {
            text += minutesSeconds(minutes, seconds);
        } else {
            text += hours + ":";
            text += minutesSeconds(minutes, seconds);
        }

        return text;
    }

    private String minutesSeconds (int minutes, int seconds) {
        String text = "";
        if (minutes < 10) text += "0";
        text += minutes + ":";
        if (seconds < 10) text += "0";
        text += seconds;
        return text;
    }


    private void setSuccess(){
        TextView success = findViewById(R.id.success);
        if (gc != null) {
            if(gc.bombDisarmed()){
                success.setText("Success!");
                TextView t = findViewById(R.id.timeout);
                t.setText("");
            }else{
                success.setText("Faliure :(");

            }
        } else {
            success.setText("No gameController found :(");  // Avoid crashing
        }



    }

    public void showHomePage() {
        Intent intent = new Intent(this, StartPage.class);
        startActivity(intent);
    }

}
