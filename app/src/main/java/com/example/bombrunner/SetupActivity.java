package com.example.bombrunner;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.Vibrator;
import android.text.Selection;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Serializable;

public class SetupActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.bombrunner.MESSAGE";
    private MediaPlayer click;
    private Vibrator v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
    }

    public void startRun (View view) {
        click = MediaPlayer.create(this, R.raw.click);
        v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

        Intent intent = new Intent (this, MapsActivity.class);

        EditText duration = findViewById(R.id.durationField);
        EditText distance = findViewById(R.id.distance_field);
        EditText hints = findViewById(R.id.hints_field);

        TextView durationWarning = findViewById(R.id.duration_warning);
        TextView distanceWarning = findViewById(R.id.distance_warning);
        TextView hintsWarning = findViewById(R.id.hints_warning);

        validate(duration, durationWarning, intent);

        validate(distance, distanceWarning, intent);

        validate(hints, hintsWarning, intent);

        if (isValid(duration.getText().toString()) &&
                isValid(distance.getText().toString()) &&
                isValid(hints.getText().toString())) {
            click.start();
            v.vibrate(50);
            int hint = Integer.parseInt(hints.getText().toString());
            int dist = Integer.parseInt(distance.getText().toString());

            GameController gc = new GameController(hint, dist); //Vi behöver läsa in antal hints och distance
            intent.putExtra("GameController", gc);
            startActivity(intent);
        }
    }

    private void validate (EditText textField, TextView warning, Intent intent) {
        String field = getField(textField);

        if (!isValid(textField.getText().toString())) {
            warning.setVisibility(View.VISIBLE);
            v.vibrate(500); //om användaren skriver in något felaktigt, blir en längre vibration
        } else {
            intent.putExtra(field, textField.getText().toString());
            warning.setVisibility(View.INVISIBLE);
        }
    }

    private String getField (EditText textField) {
        if (textField.getId() == R.id.durationField) {
            return "Duration";
        } else if (textField.getId() == R.id.distance_field) {
            return "Distance";
        } else if (textField.getId() == R.id.hints_field) {
            return "Hints";
        }

        return "";  // Should never come to this
    }

    private boolean isValid (String string) {
        return !(string.equals("") || string.equals("0"));
    }
}
